{
    "id": "3ad9933a-6770-4c52-b478-a3b1c3bb8112",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fint_score",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Verdana",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "244638a2-87bd-4d43-96aa-fd3a40789694",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 99,
                "y": 86
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "7a5db516-4bf5-450e-b715-99941ab64a43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 113,
                "y": 86
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "92ffe70f-05a9-48a4-b94e-a87f3965baac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 208,
                "y": 65
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "868d3cea-2875-40c8-ba03-98e733202798",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 19,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 128,
                "y": 23
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "7b00b814-8e36-4fc4-9081-b50187d118fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 131,
                "y": 44
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "74a56f90-915a-4f12-aa3b-425e7911e8ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 19,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "11bf7ec6-e062-448f-8bdb-b16a4dcacedf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 19,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 78,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "c57aae21-604f-4570-a82d-584ebecf62bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 143,
                "y": 86
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "09e82081-6e99-4501-8534-2f9562973b02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 21,
                "y": 86
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "e8605ffd-1267-4be8-8343-f8df9c471e30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 66,
                "y": 86
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "a11b4ca2-7cf4-4905-86ac-ca0864fca2a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 132,
                "y": 65
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "91be8ae2-f6c6-4449-b0c2-1f42b986ffda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 19,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 114,
                "y": 23
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "c0790c69-fa8e-4c1b-bee5-fe9c37ac5235",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 106,
                "y": 86
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "3136ccb8-e795-4d1a-ba00-d5987c3aeb24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 39,
                "y": 86
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "aa7bd65c-e561-45b9-8edc-7fe964240933",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 131,
                "y": 86
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "8792e9cf-ca43-49bf-a391-4de9d6ab78b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 86,
                "y": 65
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "7ae8ccdd-c55f-48bf-b6d0-c459bc2f241b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 15,
                "y": 44
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "11676a10-d613-454d-ab1a-e2a9013309d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 19,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 198,
                "y": 65
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "60b50b69-f08d-40d4-8338-7fde0d281c27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 62,
                "y": 65
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "d405aab1-10ca-4413-a952-44add553b22b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 50,
                "y": 65
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "e9d6b77e-1ba0-4f27-b9b3-fa4ee5bc06f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 80,
                "y": 44
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "fe3ccc61-ce12-42be-8edf-935f37ce119a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 98,
                "y": 65
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "3b3d5785-3568-43fb-8fba-178339adc56d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 26,
                "y": 65
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "5934aa30-1c41-447c-9ba5-f3527537081c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 38,
                "y": 65
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "565b2cbf-a757-436e-9031-bb13e2b0aaf4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 28,
                "y": 44
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "87fc8a77-df8f-4d17-915e-79e28737b33a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 41,
                "y": 44
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "c1155777-f2b2-4bf3-8483-23b258366afa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 125,
                "y": 86
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "84daee75-b6ef-4618-b546-0d640a58ed39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 75,
                "y": 86
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "7f51bcd6-b15f-46b7-b670-6608140b2092",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 19,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 86,
                "y": 23
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "a436a5d3-d2aa-4708-8260-c03b319c4b59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 19,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 106,
                "y": 44
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "26605ce5-6451-4e16-8e38-0bead7ee157b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 19,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 72,
                "y": 23
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "a347bdc5-fa64-4ff5-97bb-adf37b77c547",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 2,
                "y": 86
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "cf6fd717-5e93-4b41-a7d6-56c3f8fa8352",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 19,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "dd96e8a9-645d-4d60-bb2d-5c30b27ad786",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 126,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "f6cbe516-c1ee-4a02-b4f5-ae484f85941d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 2,
                "y": 44
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "646770b1-2dec-43b3-88a2-fc8bf6c4c803",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 233,
                "y": 23
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "cafc3242-36e4-4133-82b9-e21bae32a4a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 19,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 44,
                "y": 23
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "aa735f42-99ef-4387-b4c7-261d9563ca34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 154,
                "y": 65
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "6bd986e1-4f18-444c-b6c9-9c5a50a68f15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 110,
                "y": 65
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "7a60e84e-cbbc-49d9-9e22-90695ad0051a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 30,
                "y": 23
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "635a4d85-4bc9-4543-964b-2522c089cc76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 19,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 207,
                "y": 23
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "477246b7-c47f-40bc-8d08-5b0a177fa62c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 238,
                "y": 65
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "84f98ed0-d2ee-4a94-8ce4-04576bccbc64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 218,
                "y": 65
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "b2ebb7d7-0336-49f4-b922-7e3c7bfef6c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 12,
                "x": 16,
                "y": 23
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "478f9824-6dd9-48da-9148-276382a057c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 187,
                "y": 65
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "1a05cae1-8f33-4076-b9f6-474bdfac5cfa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 19,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 141,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "9b8dbf33-b3b9-4fa0-b879-96af0a0a8995",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 19,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 230,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "08961cf1-f831-4cc8-92fc-8a5aceb59ae8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 19,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 111,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "2e7c098f-d0b9-4392-a536-94e76b38a3e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 194,
                "y": 23
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "8fb35457-1dcc-48c8-9db1-0954d73a834f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 19,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 156,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "e97ddb23-abc6-4ea4-9c9d-5f837a19c1e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 19,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 216,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "ae671db0-7d79-4e8f-86fe-3f475676dd81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 181,
                "y": 23
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "5e8dd242-65f9-4ab0-9af2-efae441aca75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 168,
                "y": 23
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "979f8516-26c3-437d-8513-63b270270831",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 19,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 155,
                "y": 23
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "3077318e-1e42-476a-8079-0844b3ba9a98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 171,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "7badf56a-fa7b-4bf3-88ba-1f563af51cff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 19,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 23,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "a907ba77-f741-4d76-a7a0-0d21547b89f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 186,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "c60e918a-61b8-42f9-bb98-550c9e1aa279",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 2,
                "y": 23
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "8a809211-fbb2-4d24-ad08-1bc101987ac3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 142,
                "y": 23
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "f4f1e575-4c8e-4a7b-92eb-f69d01e27892",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 48,
                "y": 86
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "21b4c8dd-6c26-4e5f-8135-d98d016e3e83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 215,
                "y": 44
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "35fc1e51-511b-42a5-8d4c-8e46ad690724",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 91,
                "y": 86
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "088026f5-2c1f-4e96-9871-41ab240f58ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 19,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 58,
                "y": 23
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "427a5668-f2ba-4f8b-b042-65421321682f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 19,
                "offset": -1,
                "shift": 11,
                "w": 13,
                "x": 201,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "a67ca12b-d0e7-47dc-b777-ad381e94c670",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 19,
                "offset": 2,
                "shift": 11,
                "w": 6,
                "x": 83,
                "y": 86
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "b2ee9991-1d4c-411d-ba78-aefb1e6890dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 143,
                "y": 44
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "dbb55936-96d1-4456-95f2-5e7c2c49e3ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 155,
                "y": 44
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "8fec2656-8d29-4a37-9a13-7cc6f743c4da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 176,
                "y": 65
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "d15748b5-b645-4d5c-b5af-6b9360e32bb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 167,
                "y": 44
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "548b41a4-0b97-4995-9718-f09b6737d329",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 179,
                "y": 44
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "e23a4437-fa71-4de0-b4f4-b3230c65ab87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 228,
                "y": 65
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "371eb2e7-626f-4ede-8bf4-b9ed7189bee4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 191,
                "y": 44
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "27382551-3f33-4f31-a2e6-bb0aa5db2817",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 203,
                "y": 44
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "15406f69-6f3f-4892-9b7b-535ed6c9fff2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 119,
                "y": 86
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "03e8b822-d4fa-46a9-b176-2a2a4049659a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 19,
                "offset": -1,
                "shift": 6,
                "w": 7,
                "x": 12,
                "y": 86
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "2df367d1-e2e7-4d80-801a-3f51bd1c6a93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 227,
                "y": 44
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "916dfd6a-9931-43c6-9777-25344a407509",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 137,
                "y": 86
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "83f133a2-48b4-4207-a399-030c073ba3ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 19,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "69f14884-69c2-4eee-a4e2-9e3715475525",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 239,
                "y": 44
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "4e15b185-68f9-4c07-ba9a-fb4781ce3be1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 220,
                "y": 23
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "bc373016-dfa8-494e-8110-45cf78cb3492",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 14,
                "y": 65
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "fd7f3f0f-1ddd-4aad-832c-bb0c716b5146",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 119,
                "y": 44
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "bc15fe33-9c68-4965-a43e-1354b6e843c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 57,
                "y": 86
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "627784cd-9e4c-4a05-86f3-26aec168c26d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 121,
                "y": 65
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "5ddbbc69-9fcf-40ce-848c-c989cab266e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 30,
                "y": 86
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "b47ce792-180f-4c62-8c8d-8e589d588299",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 2,
                "y": 65
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "ac4b6f48-d90d-41d5-abbe-3eec9f4da03c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 93,
                "y": 44
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "ae51e9e3-2799-4541-9f44-ca67a9389b57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 43,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "a6f9e0a2-d547-418c-88fb-7b2e06b891ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 67,
                "y": 44
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "4227d752-7f4c-48b6-993e-2747403f2f5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 54,
                "y": 44
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "20bc9920-2137-41a6-9749-6aded6a1a050",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 165,
                "y": 65
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "6a6f8f76-5eee-46ab-8db9-a88f66b63d66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 143,
                "y": 65
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "905b1f3e-7b2b-452a-81d0-08a8d1f80d24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 19,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 149,
                "y": 86
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "cdfd651d-46b1-458e-a5d2-3039f61ae074",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 74,
                "y": 65
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "4473cd34-90f4-4d24-8b0e-d340405b99b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 19,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 100,
                "y": 23
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "score: 25",
    "size": 12,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}