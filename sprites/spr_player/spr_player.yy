{
    "id": "5febaeb7-b43f-4ad5-86a6-b808d3b59c0e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 85,
    "bbox_left": 26,
    "bbox_right": 84,
    "bbox_top": 30,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9ebc133b-caab-4f51-ba3b-df99b8ded08f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5febaeb7-b43f-4ad5-86a6-b808d3b59c0e",
            "compositeImage": {
                "id": "e7e5743a-8ee2-49bc-9979-62212f848728",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ebc133b-caab-4f51-ba3b-df99b8ded08f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de853af4-4369-42e4-b968-e30993399ba2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ebc133b-caab-4f51-ba3b-df99b8ded08f",
                    "LayerId": "1a565cd6-000f-430c-8674-2d162d91dbb0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 115,
    "layers": [
        {
            "id": "1a565cd6-000f-430c-8674-2d162d91dbb0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5febaeb7-b43f-4ad5-86a6-b808d3b59c0e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 114,
    "xorig": 57,
    "yorig": 57
}