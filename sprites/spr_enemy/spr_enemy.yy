{
    "id": "748f871e-8657-4405-8e40-01888a30ad7c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 89,
    "bbox_left": 26,
    "bbox_right": 83,
    "bbox_top": 32,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1627c2ab-8eb0-45ad-b7f3-56f5266b5634",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "748f871e-8657-4405-8e40-01888a30ad7c",
            "compositeImage": {
                "id": "bc6599ae-8bc3-4e5b-a616-e37db3656df8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1627c2ab-8eb0-45ad-b7f3-56f5266b5634",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfe1e08a-4cf1-4ab5-9399-8da39072f1b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1627c2ab-8eb0-45ad-b7f3-56f5266b5634",
                    "LayerId": "a7138266-8e3d-49a9-a746-a847bddf09ea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 114,
    "layers": [
        {
            "id": "a7138266-8e3d-49a9-a746-a847bddf09ea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "748f871e-8657-4405-8e40-01888a30ad7c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 115,
    "xorig": 57,
    "yorig": 57
}