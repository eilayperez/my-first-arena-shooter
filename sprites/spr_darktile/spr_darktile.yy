{
    "id": "70ad0f39-24de-4cae-9e45-b839c8299853",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_darktile",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "884b572a-d8a0-4aab-b821-ff6ac9173bf7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70ad0f39-24de-4cae-9e45-b839c8299853",
            "compositeImage": {
                "id": "c7bc33f8-28a5-489d-943f-89841b1310a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "884b572a-d8a0-4aab-b821-ff6ac9173bf7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "423296e0-d080-41e9-bd03-64ef03cec75f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "884b572a-d8a0-4aab-b821-ff6ac9173bf7",
                    "LayerId": "a6dc2b03-6189-4681-bed2-e54444adcdc6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "a6dc2b03-6189-4681-bed2-e54444adcdc6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "70ad0f39-24de-4cae-9e45-b839c8299853",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}