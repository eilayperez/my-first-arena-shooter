{
    "id": "83edd085-0d57-453d-b738-d158aaa94e45",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 70,
    "bbox_left": 95,
    "bbox_right": 110,
    "bbox_top": 56,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 239,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "52cdfdfc-8b63-448f-bbfc-b2afc4ab648f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83edd085-0d57-453d-b738-d158aaa94e45",
            "compositeImage": {
                "id": "8746b67b-9de0-4eb0-987f-d5920b8375fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52cdfdfc-8b63-448f-bbfc-b2afc4ab648f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2edea850-8706-4294-b84e-2429c52f9e0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52cdfdfc-8b63-448f-bbfc-b2afc4ab648f",
                    "LayerId": "5dc57a5b-6a0c-45f9-9552-43e8a29db70e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "5dc57a5b-6a0c-45f9-9552-43e8a29db70e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "83edd085-0d57-453d-b738-d158aaa94e45",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 102,
    "yorig": 62
}