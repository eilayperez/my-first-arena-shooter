{
    "id": "a59f5679-5eef-4e2e-9565-6379e40e924e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_background",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 383,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2f3931af-904b-4435-a861-7463aebac62b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a59f5679-5eef-4e2e-9565-6379e40e924e",
            "compositeImage": {
                "id": "116d1fd8-d126-41e4-a410-99ea6f73a3d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f3931af-904b-4435-a861-7463aebac62b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4ca66b8-e8b6-452c-8f74-fb0a1305998a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f3931af-904b-4435-a861-7463aebac62b",
                    "LayerId": "9e285aef-c3b7-4cf0-aac7-92d15e8ed108"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "9e285aef-c3b7-4cf0-aac7-92d15e8ed108",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a59f5679-5eef-4e2e-9565-6379e40e924e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 384,
    "xorig": 196,
    "yorig": 204
}