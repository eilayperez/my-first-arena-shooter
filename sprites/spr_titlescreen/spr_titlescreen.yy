{
    "id": "265b6042-e36c-4ea5-bb52-9fc570e69287",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_titlescreen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 282,
    "bbox_left": 0,
    "bbox_right": 859,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b8d31ab7-5b77-46c1-b97d-7bf94ccba17b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "265b6042-e36c-4ea5-bb52-9fc570e69287",
            "compositeImage": {
                "id": "7b6537cc-23a7-4184-9c0c-47407033ea12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8d31ab7-5b77-46c1-b97d-7bf94ccba17b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cde72c2f-ca6a-4cc6-98ea-506b57dd9499",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8d31ab7-5b77-46c1-b97d-7bf94ccba17b",
                    "LayerId": "636eb648-8676-457f-aa7e-f061246f9bce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 283,
    "layers": [
        {
            "id": "636eb648-8676-457f-aa7e-f061246f9bce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "265b6042-e36c-4ea5-bb52-9fc570e69287",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 860,
    "xorig": 430,
    "yorig": 141
}