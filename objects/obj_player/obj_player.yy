{
    "id": "ebfd73e8-99d0-493f-9c71-282dc8b19d25",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "e78fd824-ef0a-400a-91c4-1aa4c3fc3acd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 39,
            "eventtype": 5,
            "m_owner": "ebfd73e8-99d0-493f-9c71-282dc8b19d25"
        },
        {
            "id": "77f7da16-9d9e-4239-b20e-a1a49ebc7e96",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 37,
            "eventtype": 5,
            "m_owner": "ebfd73e8-99d0-493f-9c71-282dc8b19d25"
        },
        {
            "id": "ad45db66-1a81-4ffe-9626-5fe7ceb64bf6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 38,
            "eventtype": 5,
            "m_owner": "ebfd73e8-99d0-493f-9c71-282dc8b19d25"
        },
        {
            "id": "1380fd98-19b0-4fe4-97f1-a0c081c8ff63",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 5,
            "m_owner": "ebfd73e8-99d0-493f-9c71-282dc8b19d25"
        },
        {
            "id": "cb9062a9-3b89-40d5-82e6-1bf41ceb7535",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ebfd73e8-99d0-493f-9c71-282dc8b19d25"
        },
        {
            "id": "e677d87b-231c-4a5e-9b0f-1d4fe5faee10",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ebfd73e8-99d0-493f-9c71-282dc8b19d25"
        },
        {
            "id": "91df5b40-3612-46e0-ace7-fb22e3a5b5a4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "633e9ab5-6365-417f-a70b-c51976fbcfb5",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ebfd73e8-99d0-493f-9c71-282dc8b19d25"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5febaeb7-b43f-4ad5-86a6-b808d3b59c0e",
    "visible": true
}