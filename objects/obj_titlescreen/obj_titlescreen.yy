{
    "id": "10ecc8f8-999e-4d4d-b973-a75d550d02af",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_titlescreen",
    "eventList": [
        {
            "id": "b2872a8f-bbad-4541-831f-5e034d26ba90",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "10ecc8f8-999e-4d4d-b973-a75d550d02af"
        },
        {
            "id": "a05ac413-4294-4798-ab10-1b36b27b7d8f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "10ecc8f8-999e-4d4d-b973-a75d550d02af"
        },
        {
            "id": "1dac0c49-e898-4432-9fe7-31b49baafd19",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 10,
            "m_owner": "10ecc8f8-999e-4d4d-b973-a75d550d02af"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "265b6042-e36c-4ea5-bb52-9fc570e69287",
    "visible": true
}