/// @DnDAction : YoYo Games.Instances.If_Instance_Exists
/// @DnDVersion : 1
/// @DnDHash : 0E3ADFAF
/// @DnDArgument : "obj" "obj_player"
/// @DnDSaveInfo : "obj" "ebfd73e8-99d0-493f-9c71-282dc8b19d25"
var l0E3ADFAF_0 = false;
l0E3ADFAF_0 = instance_exists(obj_player);
if(l0E3ADFAF_0)
{
	/// @DnDAction : YoYo Games.Movement.Set_Direction_Point
	/// @DnDVersion : 1
	/// @DnDHash : 6867CB8B
	/// @DnDParent : 0E3ADFAF
	/// @DnDArgument : "x" "obj_player.x"
	/// @DnDArgument : "y" "obj_player.y"
	direction = point_direction(x, y, obj_player.x, obj_player.y);

	/// @DnDAction : YoYo Games.Movement.Set_Speed
	/// @DnDVersion : 1
	/// @DnDHash : 0F3457E2
	/// @DnDParent : 0E3ADFAF
	/// @DnDArgument : "speed" "spd"
	speed = spd;
}

/// @DnDAction : YoYo Games.Instances.Sprite_Rotate
/// @DnDVersion : 1
/// @DnDHash : 4DA0799B
/// @DnDArgument : "angle" "direction"
image_angle = direction;

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 3319CA1E
/// @DnDArgument : "var" "hp"
/// @DnDArgument : "op" "3"
if(hp <= 0)
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 7E865A3F
	/// @DnDApplyTo : 62bf7369-24f3-4ee1-981e-53c95fce23ef
	/// @DnDParent : 3319CA1E
	/// @DnDArgument : "expr" "5"
	/// @DnDArgument : "expr_relative" "1"
	/// @DnDArgument : "var" "thescore"
	with(obj_score) {
	thescore += 5;
	
	}

	/// @DnDAction : YoYo Games.Audio.Audio_Set_Pitch
	/// @DnDVersion : 1
	/// @DnDHash : 4AA9256D
	/// @DnDParent : 3319CA1E
	/// @DnDArgument : "sound" "snd_death"
	/// @DnDArgument : "pitch" "random_range(0.8,1.2) "
	/// @DnDSaveInfo : "sound" "4b0d32e0-4c92-456e-a625-b64ac8470622"
	audio_sound_pitch(snd_death, random_range(0.8,1.2) );

	/// @DnDAction : YoYo Games.Audio.Play_Audio
	/// @DnDVersion : 1
	/// @DnDHash : 1447CBC4
	/// @DnDParent : 3319CA1E
	/// @DnDArgument : "soundid" "snd_death"
	/// @DnDSaveInfo : "soundid" "4b0d32e0-4c92-456e-a625-b64ac8470622"
	audio_play_sound(snd_death, 0, 0);

	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 78E8FE59
	/// @DnDParent : 3319CA1E
	instance_destroy();
}