{
    "id": "6ad047fb-4f0e-4a0b-aca6-1a893d69e133",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bullet",
    "eventList": [
        {
            "id": "92257dee-740f-44a0-b6ff-e3c82cb08536",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6ad047fb-4f0e-4a0b-aca6-1a893d69e133"
        },
        {
            "id": "d9624ac4-850a-409c-9499-255bef58e92b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "633e9ab5-6365-417f-a70b-c51976fbcfb5",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "6ad047fb-4f0e-4a0b-aca6-1a893d69e133"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "83edd085-0d57-453d-b738-d158aaa94e45",
    "visible": true
}